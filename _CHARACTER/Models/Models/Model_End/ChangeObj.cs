﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeObj : MonoBehaviour
{
    public Transform[] listfather;

    public void Start()
    {
        int id = 0;
        for (int i = 0; i < listfather.Length; i++)
        {
            listfather[i].name = "Cube (" + id + ")";
            id++;
        }
    }

}
